import { Todo } from '@chema-sl/data';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'chema-sl-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent {
  @Input() todos: Todo[] = [];
}
